import Model.Book;
import Model.Library;
import Model.RefBook;
import Model.Student;


public class TestCase {

	public static void main(String[] args) {
		Library b = new Library();
		Student student = new Student("5610450349","Sirada");
		Book book1 = new Book("Algorithm","A.Sirikorn",2011);
		Book book2 = new Book("Software constructor","A.Chu",1999);
		RefBook refBook = new RefBook("News", "KomChatLuk");
		RefBook refBook2 = new RefBook("Magazine", "Image");
		b.addBook(book1); b.addBook(book2);
		b.addRefBook(refBook);b.addRefBook(refBook2);
		System.out.println("shelf(Book) : "+b.getCount()+" : "+b.showBooks());
		System.out.println("shelf(News&Magazine): "+b.getRefBookCount());
		System.out.println("Borrowed : Software constructor");
		b.borrowBook(student, "Software constructor");
		System.out.println("book in shelf : "+b.getCount()+" : "+b.showBooks());
	}

}
