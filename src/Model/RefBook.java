package Model;

public class RefBook {
	private String kindOfBook;
	private String title;
	
	public RefBook(String kindOfBook,String title){
		this.kindOfBook = kindOfBook;
		this.title = title;
	}

	public String getkindOfBook(){
		return kindOfBook;
	}
	
	public void settitle(String kindOfBook){
		this.kindOfBook = kindOfBook;
	}
	
	public String gettitle(){
		return title;
	}
	
	public void setauther(String title){
		this.title = title;
	}
}
