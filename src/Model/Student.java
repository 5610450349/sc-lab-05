package Model;

import java.util.ArrayList;

public class Student {
	private String name;
	private String id;
	private ArrayList<Book> books = new ArrayList<Book>();
	
	public Student(String name,String id){
		this.name = name;
		this.id = id;
	}
	
	public String getID(){
		return id;
	}
	
	public void setID(String id){
		this.id = id;
	}
	
	public String getname(){
		return name;
	}
	
	public void setname(String name){
		this.name = name;
	}
	
	
	public void borrowBook(Book book){
		books.add(book);
	}

}
