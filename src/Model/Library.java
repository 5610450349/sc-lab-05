package Model;

import java.util.ArrayList;
import Model.RefBook;

public class Library {
	ArrayList<Book> books = new ArrayList<Book>();
	ArrayList<RefBook> refBooks = new ArrayList<RefBook>();
	ArrayList<Book> borrowedBook = new ArrayList<Book>();

	public void addBook(Book book){
		books.add(book);
	}
	
	public void addRefBook(RefBook book){
		refBooks.add(book);
	}
	
	public void borrowBook(Student student,String title){
		int count = -1;
		for(int i=0;i<books.size();i++){
			if(books.get(i).getnameBook().equals(title)){
				count = i;
			}
		}	
			
		if(count!=-1){
			student.borrowBook(books.get(count));
			borrowedBook.add(books.get(count));
			books.remove(count);
		}
		
	}
	
	
	public int getCount(){
		return books.size();
	}
	
	public int getRefBookCount(){
		return refBooks.size();
	}
	
	public String showBooks(){
		String allBooks = "";
		for(Book book:books){
			allBooks += book.getnameBook()+",";
		}
		return allBooks.substring(0,allBooks.length()-1);
	}

}
